package com.kstarmp3.musicplayer;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
 Button playBtn;
 SeekBar positionBar, volumeBar;
 TextView elapsedTime, remainingTime;
 MediaPlayer media_player;

 int totalTime;

 @Override
 protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  setContentView(R.layout.activity_main);

  playBtn       = (Button) findViewById(R.id.playBtn);
  elapsedTime   = (TextView) findViewById(R.id.elapsedTime);
  remainingTime = (TextView) findViewById(R.id.remainingTime);

  // Media player
  media_player = MediaPlayer.create(this, R.raw.sweet_arms_trust_in_you);
  media_player.setLooping(true);
  media_player.seekTo(0);
  media_player.setVolume(0.5f, 0.5f);

  totalTime = media_player.getDuration();

  // Position bar
  positionBar = (SeekBar) findViewById(R.id.positionBar);
  positionBar.setMax(totalTime);

  positionBar.setOnSeekBarChangeListener(
   new SeekBar.OnSeekBarChangeListener() {
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
     if (fromUser) {
      media_player.seekTo(progress);
      positionBar.setProgress(progress);
     }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) { }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) { }
   }
  );

  // Volume bar
  volumeBar = (SeekBar) findViewById(R.id.volumeBar);

  volumeBar.setOnSeekBarChangeListener(
   new SeekBar.OnSeekBarChangeListener() {
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
     float volumeNum = progress / 100f;
     media_player.setVolume(volumeNum, volumeNum);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) { }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) { }
   }
  );

  // Thread (Update positionBar & timeLabel)
  new Thread(new Runnable() {
   @Override
   public void run() {
    while (media_player != null) {
     try {
      Message msg = new Message();
      msg.what = media_player.getCurrentPosition();
      handler.sendMessage(msg);
      Thread.sleep(1000);
     } catch (InterruptedException e) {}
    }
   }
  }).start();
 }

 private Handler handler = new Handler() {
  @Override
  public void handleMessage(Message msg) {
   int currentPosition = msg.what;

   // Update positionBar
   positionBar.setProgress(currentPosition);

   // Update labels
   String elapsed = getTime(currentPosition);
   elapsedTime.setText(elapsed);

   String remaining = getTime(totalTime - currentPosition);
   remainingTime.setText("- " + remaining);
  }
 };

 public String getTime(int time) {
  String time_label = "";

  int min, sec;

  min = (time / 1000) / 60;
  sec = (time / 1000) % 60;

  time_label = min + ":";

  if (sec < 10) time_label += "0";

  time_label += sec;

  return time_label;
 }

 // Play button
 public void btnPlay(View view) {
  if (!media_player.isPlaying()) {
   // Stopping
   media_player.start();
   playBtn.setBackgroundResource(R.drawable.stop);
  }

  else {
   // Playing
   media_player.pause();
   playBtn.setBackgroundResource(R.drawable.play);
  }
 }
}
